# Fedi Follower Export
Script(s) made for ease of exporting follower list (People that follow you)

## Why?
It seems that there's no "easy" way of getting follower list out of current versions of various Fediverse server frontends. 
So I made this script just for this, currently supports Mastodon, Pleroma and Misskey, including their derivatives (but probably not all of them, i don't know)

## Getting started
Install requirements with `python3 -m pip install -r requirements.txt`

First run requires you to run `python3 main.py -s instance.domain` with `instance.domain` being your instance. Defaults to `toot.site` because that's what I was testing this script on lmao

Depending on whether your instance supports Mastodon API or Misskey API, you will be asked for OAuth token, or your username, respectively.

For another instance, run `python3 main.py -s instance.domain -c config2.json`, you don't need to specify instance after first run

File itself is generated under `followers_list_{current_date}.csv` and is of similar format as following list export csv, meaning you can import this as a following list, if you want to for some reason.

Script will prompt you to authorize every time it won't detect the given config file. 

## Issues
~~Misskey API instances seem to not expose whole list while using API, for example, website shows 318 followers, but script finds just 284 of them. I am not sure why is that happening~~

Misskey might have a bug regarding faulty follower count, so number of rows in the file, and the number from the website will most likely be off.


## TODO
probably move to `csv` module instead of just doing `f.write(acct + "\n")` but I mean it works so idc
