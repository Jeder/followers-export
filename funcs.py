import csv
import json
from datetime import datetime
import requests
from time import sleep

now = datetime.now()
file_date = now.strftime("%Y-%m-%d_%H_%M_%S")


def mastoapi(cfg):
    headers = {
        "Authorization": f"Bearer {cfg['secret']}",
    }
    user = requests.get(f"https://{cfg['site']}/api/v1/accounts/verify_credentials", headers=headers).json()
    uid = user["id"]
    flwrs_list = requests.get(f"https://{cfg['site']}/api/v1/accounts/{uid}/followers", headers=headers)
    instance = cfg['site']
    next_url = flwrs_list.links.get("next")
    file = f"followers_list_{file_date}.csv"
    if next_url:
        next_url = next_url["url"]
    with open(file, "w"):
        pass
    print(f"Writing to {file}")
    if not next_url:
        flwrs = flwrs_list.json()
        with open(file, "a") as f:
            for i in flwrs:
                f.write(i["acct"] + "\n")
    while next_url:
        next_url = flwrs_list.links.get("next")
        flwrs = flwrs_list.json()
        with open(file, "a") as f:
            for i in flwrs:
                if "@" not in i["acct"]:
                    acct = i["acct"] + "@" + instance
                else:
                    acct = i["acct"]
                f.write(acct + "\n")
        if next_url:
            next_url = next_url["url"]
        else:
            print("Done.")
            break
        flwrs_list = requests.get(next_url, headers=headers)
        sleep(2)


def misskeyapi(cfg):
    data = {
        "username": cfg["user"],
        "host": None,
        "limit": 100
    }
    flwrs_list = requests.post(f"https://{cfg['site']}/api/users/followers", data=json.dumps(data))
    flwrs = flwrs_list.json()
    file = f"followers_list_{file_date}.csv"
    instance = cfg['site']
    with open(file, "w"):
        pass
    print(f"Writing to {file}")
    while True:
        for i in flwrs:
            with open(file, "a") as f:
                username = i["follower"]["username"]
                host = i["follower"]["host"]
                id = i["id"]
                if host is None:
                    acct = username + "@" + instance
                else:
                    acct = username + "@" + host
                f.write(acct + "\n")
        data["untilId"] = id
        flwrs_list = requests.post(f"https://{cfg['site']}/api/users/followers", data=json.dumps(data))
        flwrs = flwrs_list.json()
        if not flwrs:
            print("Done.")
            break