#!/usr/bin/env python3
import argparse
import requests
import json
import sys

import funcs


def _config():
    # config handling based on https://github.com/AgathaSorceress/mstdn-ebooks/blob/master/main.py
    nodeinfo = requests.get(f"https://{args.site}/.well-known/nodeinfo").json()["links"][0]["href"]
    software = requests.get(nodeinfo).json()["software"]["name"].lower()
    # config defaults
    cfg = {
        "site": args.site,
        "software": software
    }

    try:
        cfg.update(json.load(open(args.cfg, 'r')))
    except FileNotFoundError:
        open(args.cfg, "w").write("{}")

    print(f"Using {args.cfg} as configuration file")

    print(f"Software: {cfg['software']}")

    if cfg['software'] in mastodon_api:
        scopes = 'read:accounts'

        if "client" not in cfg:
            print(f"No application info -- registering application with {cfg['site']}")
            data = {
                "client_name": "Followers Export",
                "redirect_uris": "urn:ietf:wg:oauth:2.0:oob",
                "scopes": scopes,
                "website": "https://nyaaa.jeder.pl/@jeder"
            }
            r = requests.post(f"https://{cfg['site']}/api/v1/apps", data=data).json()
            client_id = r['client_id']
            client_secret = r['client_secret']

            cfg['client'] = {
                "id": client_id,
                "secret": client_secret
            }

        if "secret" not in cfg:
            print(f"No user credentials -- logging in to {cfg['site']}")
            url = f"https://{cfg['site']}/oauth/authorize?response_type=code&client_id={cfg['client']['id']}&redirect_uri=urn:ietf:wg:oauth:2.0:oob&scope={scopes}"
            print(
                f"Open this URL and authenticate to give exporter access to your account: {url}")
            data = {
                "grant_type": "authorization_code",
                "client_id": cfg['client']['id'],
                "client_secret": cfg['client']['secret'],
                "redirect_uri": "urn:ietf:wg:oauth:2.0:oob",
                "scope": scopes,
                "code": input("Secret: ")
            }
            cfg['secret'] = requests.post(f"https://{cfg['site']}/oauth/token", data=data).json()['access_token']

        json.dump(cfg, open(args.cfg, "w+"))

        try:
            header = {
                "Authorization": f"Bearer {cfg['secret']}"
            }
            requests.get(f"https://{cfg['site']}/api/v1/accounts/verify/credentials", headers=header)
        except requests.exceptions.HTTPError:
            print(f"The provided access token in {args.cfg} is invalid. Please delete {args.cfg} and run main.py again.")
            sys.exit(1)

    elif cfg['software'] in misskey_api:
        if "user" not in cfg:
            cfg["user"] = input("Username: ")
        json.dump(cfg, open(args.cfg, "w+"))

        try:
            data = {
                "user": cfg["user"],
                "host": None,
                "limit": 1
            }
            requests.post(f"https://{cfg['site']}/api/users/followers", data=data)
        except requests.exceptions.HTTPError:
            print(f"The provided user in {args.cfg} doesn't exist. Please delete {args.cfg} and run main.py again.")
            sys.exit(1)
    else:
        print("Given instance is not supported by this script.")
        sys.exit(2)

    return cfg


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Dump list of Fediverse followers, using Mastodon API")
    parser.add_argument('-c', '--config', dest="cfg", default="config.json", nargs="?",
                        help="specify a custom location for config.json")
    parser.add_argument('-s', '--site', dest="site", default="toot.site", nargs="?",
                        help="specify an instance, default: toot.site")
    args = parser.parse_args()

    misskey_api = ["misskey", "foundkey", "calckey"]
    mastodon_api = ["mastodon", "pleroma", "akkoma", "fedibird", "hometown"]

    cfg = _config()

    if cfg["software"] in misskey_api:
        funcs.misskeyapi(cfg)
    elif cfg["software"] in mastodon_api:
        funcs.mastoapi(cfg)
    else:
        print("Given instance is not supported by this script.")
        sys.exit(2)
